drop database if exists tarjeta;
create database tarjeta;

\c tarjeta

create table comercio(
    id_comercio int,
    codigo_postal int
);

alter table comercio add constraint comercio_pk primary key (id_comercio);

create table cliente(
    id_cliente int,
    dni_cliente int,
    nombre text,
    apellido text,
    direccion text,
    telefono int,
    email text
);

alter table cliente add constraint cliente_pk primary key (id_cliente);

create table tarjeta(
    id_tarjeta int,
    numero_tarjeta text,
    id_cliente int,
    cod_seg int,
    lim_compra float,
    tarj_vigente boolean,
    tarj_vencida boolean,
    tarj_suspendida boolean
);

alter table tarjeta add constraint tarjeta_pk primary key (id_tarjeta);
alter table tarjeta add constraint tarjeta_cliente_fk foreign key (id_cliente) references cliente(id_cliente);

create table resumen(
    id_resumen int,
    id_tarjeta int,
    periodo_resumen date,
    f_vencimiento date,
    total_a_pagar int
);

alter table resumen add constraint resumen_pk primary key (id_resumen);
alter table resumen add constraint resumen_tarjeta_fk foreign key (id_tarjeta) references tarjeta (id_tarjeta);

create table compra_autorizada(
	id_compra int,
	id_comercio int,
	id_resumen int,
	fecha_compra date,
	monto float,
	descripcion text
);

alter table compra_autorizada add constraint compra_pk primary key (id_compra);
alter table compra_autorizada add constraint compra_resumen_fk foreign key (id_resumen) references resumen(id_resumen);
alter table compra_autorizada add constraint compra_comercio_fk foreign key (id_comercio) references comercio(id_comercio);

create table compra_denegada(
	id_compra int,
	id_comercio int,
	id_resumen int,
	fecha_compra date,
	monto float,
	descripcion text
);

alter table compra_denegada add constraint compra_pk primary key (id_compra);

create table alerta(
	id_alerta int,
	id_tarjeta int,
	fecha_alerta date,
	fue_atendido boolean,
	descripcion text
);

alter table alerta add constraint alerta_pk primary key (id_alerta);
alter table alerta add constraint alerta_tarjeta_fk foreign key (id_tarjeta) references tarjeta(id_tarjeta);
